# 3D Scanning Homework & Project

*holding coding homework and project work for IN2354, 3D Scanning and Motion Capture*

*Authors:* Min-Shan Luong, Felix Neumeyer, Damian Depaoli and myself


## Semester Project: 3D Hull Carving

Implementing our own hull-carving algorithm to render a 3D model from several photos of an object presented on a checkerboard, taken with a commodity camera, e.g. a smartphone.

Sample video showing the rendering of the Standford dragon - ground truth vs. ours:

![Sample Video](data/rotating_dragons.mp4)

[:link: Link to Presentation](https://www.youtube.com/watch?v=x25Vz8e877o)

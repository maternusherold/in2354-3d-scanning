# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/exercise_5_src/exercise_05_src/FreeImageHelper.cpp" "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/exercise_5_src/exercise_05_src/CMakeFiles/exercise_05.dir/FreeImageHelper.cpp.o"
  "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/exercise_5_src/exercise_05_src/main.cpp" "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/exercise_5_src/exercise_05_src/CMakeFiles/exercise_05.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CERES_EXPORT_INTERNAL_SYMBOLS"
  "GOOGLE_GLOG_DLL_DECL="
  "_DISABLE_EXTENDED_ALIGNED_STORAGE"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../../libs/eigen"
  "."
  "../../libs/Ceres/CMake/../include"
  "FORCE"
  "../../libs/glog-lib/include"
  "../../libs/Flann"
  "/usr/local/include"
  "/usr/include/eigen3"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

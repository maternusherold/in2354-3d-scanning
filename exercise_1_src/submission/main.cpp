#include <iostream>
#include <fstream>
#include <array>
#include <math.h>

#include "Eigen.h"

#include "VirtualSensor.h"

int currentFaces = 0;

struct Vertex
{
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    // position stored as 4 floats (4th component is supposed to be 1.0)
    Vector4f position;

    // color stored as 4 unsigned char
    Vector4uc color;
};

bool triangleNorm(float edgeThresh, Vertex* vertices, int idxX, int idxY, int idxZ)
{

    Vector4f diffXY = vertices[idxX].position - vertices[idxY].position;
    float normXY = diffXY.norm();

    Vector4f diffXZ = vertices[idxX].position - vertices[idxZ].position;
    float normXZ = diffXZ.norm();

    Vector4f diffZY = vertices[idxZ].position - vertices[idxY].position;
    float normZY = diffZY.norm();

    if (normXY > edgeThresh || normXZ > edgeThresh || normZY > edgeThresh)
    {
        return false;
    }

    if (vertices[idxX].position[0] == MINF || vertices[idxY].position[0] == MINF || vertices[idxZ].position[0] == MINF)
    {
        return false;
    }
    if (vertices[idxX].position[1] == MINF || vertices[idxY].position[1] == MINF || vertices[idxZ].position[1] == MINF)
    {
        return false;
    }
    if (vertices[idxX].position[2] == MINF || vertices[idxY].position[2] == MINF || vertices[idxZ].position[2] == MINF)
    {
        return false;
    }

    if (isnan(vertices[idxX].position[0]) || isnan(vertices[idxY].position[0]) || isnan(vertices[idxZ].position[0]))
    {
        return false;
    }
    if (isnan(vertices[idxX].position[1]) || isnan(vertices[idxY].position[1]) || isnan(vertices[idxZ].position[1]))
    {
        return false;
    }
    if (isnan(vertices[idxX].position[2]) || isnan(vertices[idxY].position[2]) || isnan(vertices[idxZ].position[2]))
    {
        return false;
    }
    currentFaces++;
    return true;
}

bool WriteMesh(Vertex* vertices, unsigned int width, unsigned int height, const std::string& filename)
{
    float edgeThreshold = 0.01f; // 1cm

    // TODO 2: use the OFF file format to save the vertices grid (http://www.geomview.org/docs/html/OFF.html)
    // - have a look at the "off_sample.off" file to see how to store the vertices and triangles
    // - for debugging we recommend to first only write out the vertices (set the number of faces to zero)
    // - for simplicity write every vertex to file, even if it is not valid (position.x() == MINF) (note that all vertices in the off file have to be valid, thus, if a point is not valid write out a dummy point like (0,0,0))
    // - use a simple triangulation exploiting the grid structure (neighboring vertices build a triangle, two triangles per grid cell)
    // - you can use an arbitrary triangulation of the cells, but make sure that the triangles are consistently oriented
    // - only write triangles with valid vertices and an edge length smaller then edgeThreshold

    currentFaces = 0;

    // iterate faces once for count
    for (int x = 0; x < height-1; x ++)
    {
        for (int y = 0; y < width; y ++)
        {
            triangleNorm(edgeThreshold, vertices, x*width + y, x*width + (y+width), x*width + (y+1));
            triangleNorm(edgeThreshold, vertices, x*width + (y+width), x*width + (y+width+1), x*width + (y+1));
        }
    }

    // TODO: Get number of vertices
    unsigned int nVertices = width * height;

    // TODO: Determine number of valid faces
    unsigned nFaces = currentFaces;

    // Write off file
    std::ofstream outFile(filename);
    if (!outFile.is_open()) return false;

    // write header
    outFile << "COFF" << std::endl;
    outFile << "# numVertices numFaces numEdges" << std::endl;
    outFile << nVertices << " " << nFaces << " 0" << std::endl;
    // outFile << nVertices << " " << "0" << " 0" << std::endl;

    // TODO: save vertices
    outFile << "# list of vertices" << std::endl;
    outFile << "# X Y Z R G B A" << std::endl;

    for (int idxVertices = 0; idxVertices < nVertices; idxVertices++)
    {
        // get position values
        float x = vertices[idxVertices].position[0];
        float y = vertices[idxVertices].position[1];
        float z = vertices[idxVertices].position[2];
        //float i = 1.0;

        // get RGBW values
        int rgbX = vertices[idxVertices].color[0];
        int rgbY = vertices[idxVertices].color[1];
        int rgbZ = vertices[idxVertices].color[2];
        int rgbA = vertices[idxVertices].color[3];

        if (x == MINF || y == MINF || z == MINF || isnan(x) || isnan(y) || isnan(z))
        {
            x = 0;
            y = 0;
            z = 0;
            //i = 0;
        }
        outFile << x << " " << y << " " << z << " ";
        outFile << rgbX << " " << rgbY << " " << rgbZ << " " << rgbA << std::endl;
    }

    // TODO: save valid faces
    outFile << "# list of faces" << std::endl;
    outFile << "# nVerticesPerFace idx0 idx1 idx2 ..." << std::endl;

    for (int x = 0; x < height-1; x ++)
    {
        for (int y = 0; y < width; y ++)
        {
            if(triangleNorm(edgeThreshold, vertices, x*width + y, x*width + (y+width), x*width + (y+1)))
                outFile << "3 " << x*width + y << " " << x*width + (y+width) << " " << x*width + (y+1) << std::endl;
            if(triangleNorm(edgeThreshold, vertices, x*width + (y+width), x*width + (y+width+1), x*width + (y+1)))
                outFile << "3 " << x*width + (y+width) << " " << x*width + (y+width+1) << " " << x*width + (y+1) << std::endl;
        }
    }

    // close file
    outFile.close();

    // reset faces count
    currentFaces = 0;

    return true;
}

int main()
{
    // Make sure this path points to the data folder
    std::string filenameIn = "../../../data/rgbd_dataset_freiburg1_xyz/";
    std::string filenameBaseOut = "mesh_";

    // load video
    std::cout << "Initialize virtual sensor..." << std::endl;
    VirtualSensor sensor;
    if (!sensor.Init(filenameIn))
    {
        std::cout << "Failed to initialize the sensor!\nCheck file path!" << std::endl;
        return -1;
    }

    // convert video to meshes
    while (sensor.ProcessNextFrame())
    {
        // get ptr to the current depth frame
        // depth is stored in row major (get dimensions via sensor.GetDepthImageWidth() / GetDepthImageHeight())
        float* depthMap = sensor.GetDepth();

        // get ptr to the current color frame
        // color is stored as RGBX in row major (4 byte values per pixel, get dimensions via sensor.GetColorImageWidth() / GetColorImageHeight())
        BYTE* colorMap = sensor.GetColorRGBX();

        // get depth intrinsics
        Matrix3f depthIntrinsics = sensor.GetDepthIntrinsics();
        float fX = depthIntrinsics(0, 0);
        float fY = depthIntrinsics(1, 1);
        float cX = depthIntrinsics(0, 2);
        float cY = depthIntrinsics(1, 2);

        // compute inverse depth extrinsics
        Matrix4f depthExtrinsicsInv = sensor.GetDepthExtrinsics().inverse();

        Matrix4f trajectory = sensor.GetTrajectory();
        Matrix4f trajectoryInv = sensor.GetTrajectory().inverse();

        // K_inv
        Matrix3f depthIntrinsicsInv = depthIntrinsics.inverse();

        // (E*Mcamera)_inv
        Matrix4f emInv = trajectoryInv;

        Vector4f p_world_4;

        // TODO 1: back-projection
        // write result to the vertices array below, keep pixel ordering!
        // if the depth value at idx is invalid (MINF) write the following values to the vertices array
        // vertices[idx].position = Vector4f(MINF, MINF, MINF, MINF);
        // vertices[idx].color = Vector4uc(0,0,0,0);
        // otherwise apply back-projection and transform the vertex to world space, use the corresponding color from the colormap
        Vertex* vertices = new Vertex[sensor.GetDepthImageWidth() * sensor.GetDepthImageHeight()];

        for (int idx = 0; idx < sensor.GetDepthImageHeight() * sensor.GetDepthImageWidth(); idx++)
        {

            float z = depthMap[idx];

            // depth value is valid
            if (z != MINF && !isnan(z)) {

            // compute column and row index of image
            int idxX = idx - sensor.GetDepthImageWidth() * int(idx / sensor.GetDepthImageWidth());
            int idxY = int(idx / sensor.GetDepthImageWidth());
            float y = z * (idxY);
            float x = z * idxX;

            Vector3f p_image(x,y,z);

            // std::cout << "p_image \n" << p_image << std::endl;

            // K_inv*p_image
            Vector3f p_camera = depthIntrinsicsInv*p_image;
            // assert(p_camera.cols() == 1 && p_camera.rows() == 3);

            Vector4f p_camera_4(p_camera(0),p_camera(1),p_camera(2),1.);
            // assert(p_camera_4.cols() == 1 && p_camera_4.rows() == 4);

            // std::cout << "p_camera \n" << p_camera_4 << std::endl;

            // EM_inv*p_camera
            p_world_4 = emInv*p_camera_4;

            // std::cout << "p_world_4 \n" << p_world_4 << std::endl;

            vertices[idx].position = p_world_4;

        }
        // invalid depth value
        else {
                vertices[idx].position = Vector4f(MINF, MINF, MINF, MINF);
            }
        }

        // set color value per pixel
        for (int colindex = 0; colindex < sensor.GetDepthImageWidth()*sensor.GetDepthImageHeight()*4; colindex+=4)
        {
            if (vertices[colindex/4].position == Vector4f(MINF, MINF, MINF, MINF)) {
                vertices[colindex/4].color = Vector4uc(0, 0, 0, 0);
            } else {
                vertices[colindex/4].color = Vector4uc((char)colorMap[colindex],(char)colorMap[colindex+1],
                (char)colorMap[colindex+2],(char)colorMap[colindex+3]);
            }
        }

        // write mesh file
        std::stringstream ss;
        ss << filenameBaseOut << sensor.GetCurrentFrameCnt() << ".off";
        if (!WriteMesh(vertices, sensor.GetDepthImageWidth(), sensor.GetDepthImageHeight(), ss.str()))
        {
            std::cout << "Failed to write mesh!\nCheck file path!" << std::endl;
            return -1;
        }

        // free mem
        delete[] vertices;
    }

    return 0;
}


#pragma once
#include "SimpleMesh.h"

class ProcrustesAligner {
public:
	Matrix4f estimatePose(const std::vector<Vector3f>& sourcePoints, const std::vector<Vector3f>& targetPoints) {
		ASSERT(sourcePoints.size() == targetPoints.size() && "The number of source and target points should be the "
                                                       "same, since every source point is matched with corresponding "
                                                       "target point.");

		// We estimate the pose between source and target points using Procrustes algorithm.
		// Our shapes have the same scale, therefore we don't estimate scale. We estimated rotation and translation
		// from source points to target points.

		auto sourceMean = computeMean(sourcePoints);
		auto targetMean = computeMean(targetPoints);
		
		Matrix3f rotation = estimateRotation(sourcePoints, sourceMean, targetPoints, targetMean);
		Vector3f translation = computeTranslation(sourceMean, targetMean);

		// To apply the pose to point x on shape X in the case of Procrustes, we execute:
		// 1. Translation of a point to the shape Y: x' = x + t
		// 2. Rotation of the point around the mean of shape Y: 
		//    y = R (x' - yMean) + yMean = R (x + t - yMean) + yMean = R x + (R t - R yMean + yMean)
		
		// TODO: Compute the transformation matrix by using the computed rotation and translation.
		// You can access parts of the matrix with .block(start_row, start_col, num_rows, num_cols) = elements
		Matrix4f estimatedPose = Matrix4f::Identity();

        estimatedPose.block(0, 0, 3, 3) = rotation;
        estimatedPose.block(0, 3, 3, 1) = (-1) * rotation * sourceMean + translation + sourceMean;

		return estimatedPose;
	}

private:
	Vector3f computeMean(const std::vector<Vector3f>& points) {
		// TODO: Compute the mean of input points.
		Vector3f mean = Vector3f::Zero();
		for (auto& point:points)
            mean += point;
		return mean / points.size();
	}

	static std::vector<Vector3f> meanCenter(const std::vector<Vector3f>& sourcePoints, const Vector3f& sourceMean)
    {
	    std::vector<Vector3f> pointsMeanCentered = sourcePoints;
        for (auto& point:pointsMeanCentered)
            point - sourceMean;

        return pointsMeanCentered;
    }

    /*
     * Define a custom inner matrix product as Eigen does not facilitate such for matrices saved
     * cpp vectors.
     */
    static MatrixXf customMatrixProduct(std::vector<Vector3f> X, std::vector<Vector3f> Y)
    {
        MatrixXf matX(3, 4);
        matX.col(0) = X[0];
        matX.col(1) = X[1];
        matX.col(2) = X[2];
        matX.col(3) = X[3];

        MatrixXf matY(3, 4);
        matY.col(0) = Y[0];
        matY.col(1) = Y[1];
        matY.col(2) = Y[2];
        matY.col(3) = Y[3];
        matY.transposeInPlace();

        Matrix3f prod = matX * matY;
        ASSERT( prod.rows() == 3 && prod.cols() == 3 && "Shape mismatch in result!");
        return prod;
    }

	Matrix3f estimateRotation(const std::vector<Vector3f>& sourcePoints, const Vector3f& sourceMean,
                           const std::vector<Vector3f>& targetPoints, const Vector3f& targetMean) {
		// TODO: Estimate the rotation from source to target points, following the Procrustes algorithm.
		// To compute the singular value decomposition you can use JacobiSVD() from Eigen.
		// Important: The covariance matrices should contain mean-centered source/target points.
		Matrix3f rotation = Matrix3f::Identity();

		// mean-center the matrices
		std::vector<Vector3f> X = meanCenter(sourcePoints, sourceMean);
        std::vector<Vector3f> Y = meanCenter(targetPoints, targetMean);

        // compute matrix product and SVD
        MatrixXf XX = customMatrixProduct(Y, X);
        JacobiSVD<MatrixXf> svd(XX, ComputeThinU | ComputeThinV);

        // compute SVD as in ex. slides 10 & 11
        MatrixXf V = svd.matrixV();
        MatrixXf U = svd.matrixU();
        V.transposeInPlace();
        MatrixXf R = U * V;

        // correct for deformed matrix
        if (R.determinant() == -1.)
        {
            Matrix3f I = Matrix3f::Zero();
            I(0, 0) = 1;
            I(1, 1) = 1;
            I(2, 2) = -1;
            R = U * I * V;
        }

		return R;
	}

	Vector3f computeTranslation(const Vector3f& sourceMean, const Vector3f& targetMean) {
		// TODO: Compute the translation vector from source to target points.
		Vector3f translation = Vector3f::Zero();
        translation = targetMean - sourceMean;

		return translation;
	}
};
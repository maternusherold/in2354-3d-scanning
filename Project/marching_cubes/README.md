# Marching Cubes Module

_loose module holding Marching Cubes as implemented in 3D Scanning, IN2354_

## Usage

requires to set the dimensions of a volume to be set inside `main.cpp` and the values per point 
in the volume to be provided either via cbv or as input file via `./input/volume.txt`. 

## Intendet Usage

work with the provided `Volume` in general as it already provides convenient features. Then, when 
computing the meshes, just pass the object to a dedicated function.

## TODO

 - [ ] define extra method receiving a `Volume` object  
 - [ ] init. a new `SimpleMesh` inside the above to provide to `ProcessVolumeCell` 
 - [ ] write the `mesh.off` already in that function
 - [ ] unify libraries, i.e. `./libs`

## General doc.

Usage of the different files 
 -  `Eigen.h`, `MarchingCubes.h` obvious
 - `Volume.*` provide us with a nice representation of volumes -> representation of point cloud
 - `SimpleMesh.h` mesh functionality s.a. triangulation and writing as `*.off` file 

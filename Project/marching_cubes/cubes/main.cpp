#include <iostream>
#include <fstream>

#include "Eigen.h"
#include "Volume.h"
#include "MarchingCubes.h"
#include "HullCarving.h"
#include "Helpers.h"
#include <string>
#include <opencv2/opencv.hpp>

enum VolumeInputType {
    FILEINPUT,
    RUNTIME
};

int main() {
	// Uncomment to create a ArUco board image :)
	//createArUcoBoardImage(cv::aruco::getPredefinedDictionary(cv::aruco::DICT_ARUCO_ORIGINAL), "aruco_board.png");

    // define input files
    std::string meshFile = "../../output/mesh.off";
    
    const VolumeInputType volumeInputType = RUNTIME;

    // TODO: pass volume from hull carving/projection/...
    // read the volume data from file and fill
    unsigned int mc_res = 200; // resolution of the grid, for debugging you can reduce the resolution (-> faster)
    Volume vol(Vector3d(-0.25,-0.15,-0.1), Vector3d(0.25,0.15,0.35), mc_res, mc_res, mc_res, 1);

    std::vector<Eigen::Matrix4d> cameraPoses;
    std::vector<Eigen::Matrix4d> volumeCorners;

    Eigen::Matrix4d volCorner0 = Eigen::Matrix4d::Identity();
    Eigen::Matrix4d volCorner1 = Eigen::Matrix4d::Identity();
    Eigen::Matrix4d volCorner2 = Eigen::Matrix4d::Identity();
    Eigen::Matrix4d volCorner3 = Eigen::Matrix4d::Identity();
    Eigen::Matrix4d volCorner4 = Eigen::Matrix4d::Identity();
    Eigen::Matrix4d volCorner5 = Eigen::Matrix4d::Identity();
    Eigen::Matrix4d volCorner6 = Eigen::Matrix4d::Identity();
    Eigen::Matrix4d volCorner7 = Eigen::Matrix4d::Identity();
    volCorner0.block(0, 3, 3, 1) = vol.pos(0, 0, 0);
    volCorner1.block(0, 3, 3, 1) = vol.pos(mc_res-1, 0, 0);
    volCorner2.block(0, 3, 3, 1) = vol.pos(0, mc_res - 1, 0);
    volCorner3.block(0, 3, 3, 1) = vol.pos(0, 0, mc_res - 1);
    volCorner4.block(0, 3, 3, 1) = vol.pos(mc_res - 1, mc_res - 1, 0);
    volCorner6.block(0, 3, 3, 1) = vol.pos(0, mc_res - 1, mc_res - 1);
    volCorner7.block(0, 3, 3, 1) = vol.pos(mc_res - 1, 0, mc_res - 1);
    volCorner5.block(0, 3, 3, 1) = vol.pos(mc_res - 1, mc_res - 1, mc_res - 1);
    volumeCorners.push_back(volCorner0);
    volumeCorners.push_back(volCorner1);
    volumeCorners.push_back(volCorner2);
    volumeCorners.push_back(volCorner3);
    volumeCorners.push_back(volCorner4);
    volumeCorners.push_back(volCorner5);
    volumeCorners.push_back(volCorner6);
    volumeCorners.push_back(volCorner7);

    switch (volumeInputType) {
    case FILEINPUT:
    {
        std::ifstream fileVolume("../../input/volume.txt");
        std::string line;
        for (unsigned int x = 0; x < vol.getDimX(); x++)
        {
            for (unsigned int y = 0; y < vol.getDimY(); y++)
            {
                for (unsigned int z = 0; z < vol.getDimZ(); z++)
                {
                    getline(fileVolume, line);
                    double val = std::stod(line);
                    // fill volume
                    vol.set(x, y, z, val);
                }
            }
        }
        // close file
        fileVolume.close();
    }
        break;

    case RUNTIME:
        const float F = 24;
        const Eigen::Vector2f sensorSize(36, 24);
        const Eigen::Vector2i imageSize(1920, 1080);
        const cv::Mat cameraMatrixK = cameraMatrixFromUnity(F, sensorSize, imageSize);
        std::cout << cameraMatrixK << std::endl;
        const cv::Mat_<double> distCoeff = (cv::Mat_<double>(5, 1) << 0, 0, 0, 0, 0);
        //const std::string imagePath = "C:/Greenscreenshots/dragon/97/01.png";
        const std::string imagePath = "C:/GitRepositories/in2354-3d-scanning/Project/marching_cubes/Greenscreenshots/97/0.png";
        //cv::Mat test = cv::imread(imagePath);
        //cv::imshow("test", test);
        
        vol.fill(1); // Set all voxel values to 1
        HullCarving(vol, imagePath, imageSize, cameraMatrixK, distCoeff, cameraPoses);
        break;
    }

    // TODO: put into dedicated method
    // extract the zero iso-surface using marching cubes
    SimpleMesh mesh;
    for (unsigned int x = 0; x < vol.getDimX() - 1; x++)
    {
        std::cerr << "Marching Cubes on slice " << x << " of " << vol.getDimX() << std::endl;

        for (unsigned int y = 0; y < vol.getDimY() - 1; y++)
        {
            for (unsigned int z = 0; z < vol.getDimZ() - 1; z++)
            {
                ProcessVolumeCell(&vol, x, y, z, 0.50f, &mesh, true);
            }
        }
    }

    for (const auto& cameraPose : cameraPoses) {
        Eigen::MatrixXf camPoseFloat = cameraPose.cast <float>();
        mesh = SimpleMesh::joinMeshes(SimpleMesh::sphere(camPoseFloat.block(0, 3, 3, 1), 0.02f), mesh);
    }
    for (const auto& cameraPose : volumeCorners) {
        Eigen::MatrixXf camPoseFloat = cameraPose.cast <float>();
        mesh = SimpleMesh::joinMeshes(SimpleMesh::sphere(camPoseFloat.block(0, 3, 3, 1), 0.02f, Vector4uc(0, 255, 0, 255)), mesh);
    }

    // write mesh to file
    if (!mesh.writeMesh(meshFile))
    {
        std::cout << "ERROR: unable to write output file!" << std::endl;
        return -1;
    }

    return 0;
}

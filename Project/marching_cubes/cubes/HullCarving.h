#include "Volume.h"
#include "Helpers.h"
#include <opencv2/opencv.hpp>
#include <opencv2/aruco.hpp>
#include <opencv2/core/eigen.hpp>
#include <iostream>


double interpolateIso(cv::Mat image, double x, double y);

#pragma once

cv::Mat BackgroundSegmentation(cv::Mat& imgInput)
{
    cv::Mat imgHSV;
    cv::Mat imgThreshWhite;
    cv::Mat imgThreshBlack;
    cv::Mat imgThreshGreen;
    cv::Mat imgSegmentation;

    cv::cvtColor(imgInput, imgHSV, cv::COLOR_BGR2HSV);

    // H: 0 - 179, S : 0 - 255, V : 0 - 255
    cv::inRange(imgHSV, cv::Scalar(0, 0, 0), cv::Scalar(179, 50, 255), imgThreshWhite); // low S, (high V)
    cv::inRange(imgHSV, cv::Scalar(0, 0, 0), cv::Scalar(179, 255, 60), imgThreshBlack);   // low V
    cv::inRange(imgHSV, cv::Scalar(36, 40, 20), cv::Scalar(85, 255, 255), imgThreshGreen);

    cv::add(imgThreshWhite, imgThreshBlack, imgSegmentation);
    cv::add(imgThreshGreen, imgSegmentation, imgSegmentation);

    cv::bitwise_not(imgSegmentation, imgSegmentation); // invert mask to select the object 

    cv::GaussianBlur(imgSegmentation, imgSegmentation, cv::Size(3, 3), 0);

    // Closing (Morphological Transformation)
    cv::Mat structuringElement = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
    cv::dilate(imgSegmentation, imgSegmentation, structuringElement);
    cv::erode(imgSegmentation, imgSegmentation, structuringElement);

    return imgSegmentation;
}

Eigen::Matrix3d createRotMatrix(Eigen::Vector3d x, Eigen::Vector3d y)
{
    Eigen::Matrix3d rot;
    rot.col(0) = x.normalized();
    rot.col(2) = x.cross(y).normalized();

    // calculating y by cross product ensures perfect correspondence
    Eigen::Vector3d y_new = rot.col(2).cross(x);
    rot.col(1) = y_new.normalized();
    return rot;
}

#pragma region Camera Pose Estimation

Eigen::Matrix4d cameraPoseEstimation(cv::Mat& imgInput, cv::Mat camera_matrix, std::vector<float> dist_coeffs)
{
    // set properties of tag detection (maybe put somewhere else)

    Eigen::Vector4d tag0pos;
    Eigen::Vector4d tag1pos;
    Eigen::Vector4d tag2pos;
    Eigen::Vector4d tag3pos;

    double marker_length_m;
    int dictionaryId;

    tag0pos << 0.25,0.25,0.,0.;
    tag1pos << -0.25,0.25,0.,0.;
    tag2pos << -0.25,-0.25,0.,0.;
    tag3pos << 0.25,-0.25,0.,0.;
    marker_length_m = 0.1;
    dictionaryId = 16;

    cv::Ptr<cv::aruco::Dictionary> dictionary =
        cv::aruco::getPredefinedDictionary(\
            cv::aruco::PREDEFINED_DICTIONARY_NAME(dictionaryId));

    std::vector<int> ids;
    std::vector<std::vector<cv::Point2f> > corners;
    cv::aruco::detectMarkers(imgInput, dictionary, corners, ids);

    // tag positions
    Eigen::Vector3d tag0;
    Eigen::Vector3d tag1;
    Eigen::Vector3d tag2;
    Eigen::Vector3d tag3;

    // trick: subtract 6 with index of tag in order to identify which of the four tags cannot be seen
    int unseentag = 6;

    Eigen::Matrix4d marker1to0pose;
    marker1to0pose.setIdentity();

    if (ids.size() == 3)
    {
        std::vector<cv::Vec3d> rvecs, tvecs;
        cv::aruco::estimatePoseSingleMarkers(corners, marker_length_m,
            camera_matrix, dist_coeffs, rvecs, tvecs);

        for (int i = 0; i < 3; i++)
        {
            // cout << ids[i] << " " << tvecs[i] << endl;
            switch (ids[i])
            {
            case 0:
                tag0(0) = tvecs[i][0];
                tag0(1) = tvecs[i][1];
                tag0(2) = tvecs[i][2];
                marker1to0pose.col(3)=tag0pos;

                // cout << " tag0: " << tag0 << endl;
                break;
            case 1:
                tag1(0) = tvecs[i][0];
                tag1(1) = tvecs[i][1];
                tag1(2) = tvecs[i][2];
                marker1to0pose.col(3)=tag1pos;
                // cout << " tag1: " << tag1 << endl;
                unseentag -= 1;
                break;
            case 2:
                tag2(0) = tvecs[i][0];
                tag2(1) = tvecs[i][1];
                tag2(2) = tvecs[i][2];
                marker1to0pose.col(3)=tag2pos;
                // cout << " tag2: " << tag2 << endl;
                unseentag -= 2;
                break;
            case 3:
                tag3(0) = tvecs[i][0];
                tag3(1) = tvecs[i][1];
                tag3(2) = tvecs[i][2];

                marker1to0pose.col(3)=tag3pos;
                // cout << " tag3: " << tag3 << endl;
                unseentag -= 3;
                break;
            default:
                std::cout << "tag with wrong id identified! (only use tags 0-3)" << std::endl;
            }
        }
    }
    else {
        std::cout << "not detected exactly three markers!" << std::endl;
    }

    // calculate vector x and y denoting coord. system 
    // set transformation from current center tag to world tag 
    Eigen::Vector3d transformtoworld;
    Eigen::Vector3d x;
    Eigen::Vector3d y;

    // Eigen::Vector3d worldposition;
    // worldposition << 0, 0, 0;

    Eigen::Matrix4d worldrot;

    switch (unseentag)
    {
    case 2:
        // tag 0 center
        x = tag1 - tag0;
        y = tag3 - tag0;
        worldrot.block<3, 1>(0, 3) = tag0;
        break;
    case 3:
        // worldposition += tag1pos;
        x = -1 * (tag1 - tag0);
        y = tag2 - tag1;
        worldrot.block<3, 1>(0, 3) = tag1;
        break;
    case 0:
        // worldposition += tag2pos;
        x = -1 * (tag3 - tag2);
        y = -1 * (tag1 - tag2);
        worldrot.block<3, 1>(0, 3) = tag2;
        break;
    case 1:
        // worldposition += tag3pos;
        x = (tag2 - tag3);
        y = -1 * (tag1 - tag3);
        worldrot.block<3, 1>(0, 3) = tag3;
        break;
    default:
        std::cout << "nononono you shall not pass!" << std::endl;
    }

    // Eigen::Vector4d worldposition4;
    // worldposition4 << worldposition[0], worldposition[1], worldposition[2], 0;

    worldrot.block<3, 3>(0, 0) = createRotMatrix(x, y);

    return marker1to0pose*worldrot;

    // Eigen::Vector4d pose = worldrot;
}

// alternative function only using one tag for the pose estimation
Eigen::Matrix4d cameraPoseEstimation2(cv::Mat &imgInput, cv::Mat camera_matrix, std::vector<float> dist_coeffs)
{
    // set properties of tag detection (maybe put somewhere else)

    Eigen::Vector4d tag0pos;
    Eigen::Vector4d tag1pos;
    Eigen::Vector4d tag2pos;
    Eigen::Vector4d tag3pos;

    double marker_length_m;
    int dictionaryId;

    tag0pos << 0.25,0.25,0.,1.;
    tag1pos << -0.25,0.25,0.,1.;
    tag2pos << -0.25,-0.25,0.,1.;
    tag3pos << 0.25,-0.25,0.,1.;
    marker_length_m = 0.1;
    dictionaryId = 16;

    cv::Ptr<cv::aruco::Dictionary> dictionary =
        cv::aruco::getPredefinedDictionary( \
        cv::aruco::PREDEFINED_DICTIONARY_NAME(dictionaryId));

    std::vector<int> ids;
    std::vector<std::vector<cv::Point2f> > corners;
    cv::aruco::detectMarkers(imgInput, dictionary, corners, ids);

    Eigen::Matrix4d marker1pose;

    Eigen::Matrix4d marker1to0pose;
    marker1to0pose.setIdentity();

    if (ids.size() > 0)
    {
        std::vector<cv::Vec3d> rvecs, tvecs;
        cv::aruco::estimatePoseSingleMarkers(corners, marker_length_m,
            camera_matrix, dist_coeffs, rvecs, tvecs);

                switch (ids[0])
                {
                    case 0:
                        marker1to0pose.col(3)=tag0pos;
                        break;
                    case 1:
                        marker1to0pose.col(3)=tag1pos;
                        break;
                    case 2:
                        marker1to0pose.col(3)=tag2pos;
                        break;
                    case 3:
                        marker1to0pose.col(3)=tag3pos;
                        break;
                    default:
                        std::cout << "only use tags num 1-3! _insert your sad computer noise here_" << std::endl;
                }        
        
        marker1pose = opencvPoseToTransformationMatrix(rvecs[0],tvecs[0]);

        /// Draw coordinate axes after transforming from camera to marker to world origin
        Eigen::Matrix4d worldOrigin = marker1pose * marker1to0pose;
        cv::Mat worldOriginCV;
        cv::Vec3d worldRot;
        cv::eigen2cv<double>(worldOrigin, worldOriginCV);

        cv::Mat R;
        Eigen::Matrix3d Rot = worldOrigin.block(0, 0, 3, 3);
        cv::eigen2cv<double>(Rot, R);
        cv::Rodrigues(R, worldRot);
		cv::Vec3d worldT(worldOrigin(0, 3), worldOrigin(1, 3), worldOrigin(2, 3));

        cv::aruco::drawAxis(imgInput, camera_matrix, dist_coeffs, worldRot, worldT, 0.1f);
        cv::aruco::drawAxis(imgInput, camera_matrix, dist_coeffs, rvecs[0], tvecs[0], 0.1f);
		return worldOrigin;

    } else {
        std::cout << "no marker detected! _insert your sad computer noise here_" << std::endl;
    }

    return marker1pose;

}

// new funciton using marker board
Eigen::Matrix4d cameraPoseEstimation3(cv::Mat &imgInput, cv::Mat camera_matrix, std::vector<float> dist_coeffs)
{
    // set properties of tag detection (maybe put somewhere else)

    Eigen::Vector4d arucoBoardCenter;

    double marker_length_m;
    int dictionaryId;

	arucoBoardCenter << 0.24,0.34,0.,1.;
    marker_length_m = 0.08;
    dictionaryId = 16;

    cv::Ptr<cv::aruco::Dictionary> dictionary =
        cv::aruco::getPredefinedDictionary( \
        cv::aruco::PREDEFINED_DICTIONARY_NAME(dictionaryId));

    // set detection parameters
    cv::Ptr<cv::aruco::DetectorParameters> parameters = cv::aruco::DetectorParameters::create();
    parameters->polygonalApproxAccuracyRate = 0.05;


	cv::Ptr<cv::aruco::GridBoard> board = cv::aruco::GridBoard::create(5, 7, 0.08, 0.02, dictionary);

    std::vector<int> ids;
    std::vector<std::vector<cv::Point2f> > corners;
    cv::aruco::detectMarkers(imgInput, board->dictionary, corners, ids, parameters);

    Eigen::Matrix4d arucoBoardPose;

    Eigen::Matrix4d arucoBoardCenterPose;
    arucoBoardCenterPose.setIdentity();
	arucoBoardCenterPose.col(3) = arucoBoardCenter;

    if (ids.size() > 0)
    {
        cv::Vec3f rvecs, tvecs;

        int valid = cv::aruco::estimatePoseBoard(corners, ids, board, camera_matrix, dist_coeffs, rvecs, tvecs);

		cv::aruco::drawDetectedMarkers(imgInput, corners, ids);
        
        arucoBoardPose = opencvPoseToTransformationMatrix(rvecs,tvecs);

        /// Draw coordinate axes after transforming from camera to marker to world origin
        Eigen::Matrix4d worldOrigin = arucoBoardPose * arucoBoardCenterPose;
        cv::Mat worldOriginCV;
        cv::Vec3d worldRot;
        cv::eigen2cv<double>(worldOrigin, worldOriginCV);

        cv::Mat R;
        Eigen::Matrix3d Rot = worldOrigin.block(0, 0, 3, 3);
        cv::eigen2cv<double>(Rot, R);
        cv::Rodrigues(R, worldRot);
		cv::Vec3d worldT(worldOrigin(0, 3), worldOrigin(1, 3), worldOrigin(2, 3));

        cv::aruco::drawAxis(imgInput, camera_matrix, dist_coeffs, worldRot, worldT, 0.1f);
        cv::aruco::drawAxis(imgInput, camera_matrix, dist_coeffs, rvecs, tvecs, 0.1f);
		return worldOrigin;

    } else {
        std::cout << "no marker detected! _insert your sad computer noise here_" << std::endl;
    }

    return arucoBoardPose;

}

#pragma endregion

void HullCarving(Volume &voxelGrid, const std::string inputImagePath, const Eigen::Vector2i imageSize, const cv::Mat &K, const std::vector<float> &distCoeffs, std::vector<Matrix4d> &cameraPoses)
{
    // Read images using OpenCV ImageCapture class
    cv::VideoCapture capWebcam = cv::VideoCapture();
    capWebcam.open(inputImagePath);
    if (capWebcam.isOpened() == false) // check if VideoCapture object was associated to webcam successfully
    {				
        std::cout << "error: capWebcam not accessed successfully\n\n";	// if not, print error message to std out
        return;														// and exit program
    }

    cv::Mat imgOriginal;
    cv::Mat imgUndist;
    cv::Mat imgSegmentation;

    // Get new camera matrix after undistortion
    cv::Mat mapX;
    cv::Mat mapY;
    cv::Mat newK = cv::getOptimalNewCameraMatrix(K, distCoeffs, cv::Size(imageSize.x(), imageSize.y()), 0);
    cv::initUndistortRectifyMap(K, distCoeffs, cv::Mat(), newK, cv::Size(imageSize.x(), imageSize.y()), CV_16SC2, mapX, mapY);
    Eigen::Matrix3d eigenK;
    cv::cv2eigen<double, 3, 3>(newK, eigenK);
	Eigen::Matrix3Xd identity3x4 = Matrix<double, 3, 4>::Identity();
    
    cv::namedWindow("Debug Image", cv::WINDOW_KEEPRATIO);
	cv::resizeWindow("Debug Image", cv::Size(imageSize.x() / 2, imageSize.y() / 2));

	std::cout << K << std::endl;
	std::cout << newK << std::endl;
	
    while (capWebcam.isOpened())
    {
        bool blnFrameReadSuccessfully = capWebcam.read(imgOriginal);		// get next frame

		if (!blnFrameReadSuccessfully || imgOriginal.empty()) {		// if frame not read successfully
			std::cout << "error: frame not read from webcam\n";		// print error message to std out
			break;													// and jump out of while loop
		}

        // 0.0 Undistort original image
        //cv::undistort(imgOriginal, imgUndist, K, distCoeffs);         // undistort image according to calibration. TODO possibly use cv::initUndistortRectifyMap to speed up 
        cv::remap(imgOriginal, imgUndist, mapX, mapY, cv::INTER_LINEAR);

        // 0.1 Do background foreground segmentation and find camera pose
        imgSegmentation = BackgroundSegmentation(imgUndist);

        // std::vector<cv::Vec3d> rvecs, tvecs;
        // 1. Find camera position in every image (ArUco markers)
        // TODO: find and estimate pose of aruco markers with estimatePoseSingleMarkers(); consider the distortion coefficients here!
        // Eigen::Matrix4f cameraPose = opencvPoseToTransformationMatrix(rvecs[0], tvecs[0]).inverse(); // TODO: maybe compute inverse more efficiently (since inverse of rotation matrix R^(-1) = R^T, t = -R^T*t)

        Eigen::Matrix4d cameraPose = cameraPoseEstimation3(imgUndist, newK, distCoeffs);
		Eigen::Matrix4d cameraPoseInverse = inverseTransformation(cameraPose);
		cameraPoses.push_back(cameraPoseInverse);
		Eigen::Vector4d origin(0, 0, 0, 1);
		Eigen::Vector3d originImageCoords = eigenK * identity3x4 * cameraPose * origin;
		originImageCoords /= originImageCoords.z();
		cv::circle(imgOriginal, cv::Point(originImageCoords.x(), originImageCoords.y()), 3, cv::Scalar(0, 0, 255), 1, cv::LineTypes::FILLED);

        //cameraPoses.push_back(cameraPose);
        //std::cout << cameraPose << std::endl;

        for (unsigned int x = 0; x < voxelGrid.getDimX(); x++)
        {
            for (unsigned int y = 0; y < voxelGrid.getDimY(); y++)
            {
                for (unsigned int z = 0; z < voxelGrid.getDimZ(); z++)
                {
                    Eigen::Vector3d voxelPos = voxelGrid.pos(x, y, z);
                    Eigen::Vector4d worldCoord = Eigen::Vector4d(voxelPos.x(), voxelPos.y(), voxelPos.z(), 1);
                    // 2. Transform each voxel into camera space 
                    Eigen::Vector4d cameraCoord = cameraPose * worldCoord;

                    // 3. Project voxel onto image plane via intrinsic matrix
                    Eigen::Vector3d imageCoords = eigenK * identity3x4 * cameraCoord;
					imageCoords /= imageCoords.z();

                    // Ignore voxels that were not projected into the image/are outside the view
                    if (imageCoords.x() < 1 || imageCoords.x() > imageSize.x() - 1 || imageCoords.y() < 1 || imageCoords.y() > imageSize.y() - 1)
                        continue;

                    // 4. Check corresponding image segmentation mask and set voxel to 0 or 1 
                    //    depending on whether the voxel is in the foreground or background of the segmentation
                    double iso = interpolateIso(imgSegmentation, (double)imageCoords(0),
                                                (double)imageCoords(1));
                    // Only update iso value, if it's smaller (i.e. carve away stuff)
                    if(iso < voxelGrid.get(x, y, z))
                        voxelGrid.set(x, y, z, iso);
                }
            }
        }

		cv::imshow("Debug Image", imgUndist);
		cv::waitKey(1);
    }
}

/*
 * Compute a linear interpolation between the backprojection and the surrounding pixels
 */
double interpolateIso(cv::Mat image, double x, double y) {

    // get the surrounding pixels
    int xFloor = std::floor(x);
    int xCeil = std::ceil(x);
    int yFloor = std::floor(y);
    int yCeil = std::ceil(y);

	double alpha = (x - xFloor) / (xCeil - xFloor);
	double beta = (y - yFloor) / (yCeil - yFloor);

    double pixel11 = image.at<unsigned char>(yFloor, xFloor) / 255.0;
    double pixel12 = image.at<unsigned char>(yCeil, xFloor) / 255.0;
    double pixel21 = image.at<unsigned char>(yFloor, xCeil) / 255.0;
    double pixel22 = image.at<unsigned char>(yCeil, xCeil) / 255.0;

	double interpolation = pixel11 * (1 - alpha) * (1 - beta)
		+ pixel21 * alpha * (1 - beta)
		+ pixel12 * (1 - alpha) * beta
		+ pixel22 * alpha * beta;

    return interpolation;
}

// Mat cv::getOptimalNewCameraMatrix();
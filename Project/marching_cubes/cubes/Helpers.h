#pragma once
#include "Eigen.h"
#include <opencv2/opencv.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/core/eigen.hpp>
#include <opencv2/aruco.hpp>

cv::Mat cameraMatrixFromUnity(float F, Eigen::Vector2f sensorSize, Eigen::Vector2i imageSize)
{
    float fx = F * imageSize.x() / sensorSize.x();
    float fy = F * imageSize.y() / sensorSize.y();
    cv::Mat_<float> K = (cv::Mat_<float>(3,3) << 
        fx,  0,  (imageSize.x() / 2),
         0,  fy, (imageSize.y() / 2),
         0,  0,  1);
    return K;
}

Eigen::Matrix4d opencvPoseToTransformationMatrix(cv::Vec3d rvec, cv::Vec3d tvec) {
    Eigen::Matrix4d transformation;
    Eigen::Vector4d translation(tvec[0], tvec[1], tvec[2], 1);
    cv::Mat rotMat;
    cv::Rodrigues(rvec, rotMat);
    //cv::cv2eigen<double, 4, 4>(rotMat, transformation);
    //transformation.block(0, 3, 4, 1) = translation;
    transformation << rotMat.at<double>(0, 0), rotMat.at<double>(0, 1), rotMat.at<double>(0, 2), tvec[0],
        rotMat.at<double>(1, 0), rotMat.at<double>(1, 1), rotMat.at<double>(1, 2), tvec[1],
        rotMat.at<double>(2, 0), rotMat.at<double>(2, 1), rotMat.at<double>(2, 2), tvec[2],
        0, 0, 0, 1;
    return transformation;
}

Eigen::Matrix4d inverseTransformation(Eigen::Matrix4d transformationMatrix) {
    Eigen::Matrix4d inverse;
	Eigen::Matrix3d R_inv = transformationMatrix.block(0, 0, 3, 3).transpose();
	inverse.block(0, 0, 3, 3) = R_inv;
    inverse.block(0, 3, 3, 1) = -R_inv * transformationMatrix.block(0, 3, 3, 1);
    return inverse;
}

void createArUcoBoardImage(cv::Ptr<cv::aruco::Dictionary> dictionary, std::string outputFile) {
	cv::Ptr<cv::aruco::GridBoard> board = cv::aruco::GridBoard::create(5, 7, 0.04, 0.01, dictionary);
	cv::Mat boardImage;
	board->draw(cv::Size((5*4 + 4) * 100, (7*4 + 6) * 100), boardImage, 0, 1);
	cv::imwrite(outputFile, boardImage);
}
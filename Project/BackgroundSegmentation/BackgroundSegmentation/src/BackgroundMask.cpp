#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>

#include<iostream>

///////////////////////////////////////////////////////////////////////////////////////////////////
int main() {
	cv::VideoCapture capWebcam = cv::VideoCapture();		
	std::string filename;
	//filename = "data/Greenscreenshots/01.png";	// provide the path to the first image within a folder
//////////
	if (!filename.empty()) {
		capWebcam.open(filename);
	}
	else {
		capWebcam.open(1, cv::CAP_DSHOW);			// declare a VideoCapture object and associate to webcam, 0 => use 1st webcam
		double focus = 0;
		capWebcam.set(cv::CAP_PROP_AUTOFOCUS, focus);
		capWebcam.set(cv::CAP_PROP_SETTINGS, 1);	// open camera settings window
	}
///////////
	if (capWebcam.isOpened() == false) {				// check if VideoCapture object was associated to webcam successfully
		std::cout << "error: capWebcam not accessed successfully\n\n";	// if not, print error message to std out
		return(0);														// and exit program
	}
///////////
	cv::Mat imgOriginal;		// input image
	cv::Mat imgHSV;
	cv::Mat imgThreshWhite;
	cv::Mat imgThreshBlack;
	cv::Mat imgThreshGreen;
	cv::Mat imgSegmentation;

	char charCheckForEscKey = 0;

	// declare windows
	cv::namedWindow("imgOriginal", cv::WINDOW_AUTOSIZE);	// note: you can use CV_WINDOW_NORMAL which allows resizing the window
	cv::namedWindow("imgSegmentation", cv::WINDOW_AUTOSIZE);	// or CV_WINDOW_AUTOSIZE for a fixed size window matching the resolution of the image
	cv::namedWindow("imgSegmentation without Morphology", cv::WINDOW_AUTOSIZE);
	// CV_WINDOW_AUTOSIZE is the default

	while (charCheckForEscKey != 27 && capWebcam.isOpened()) {		// until the Esc key is pressed or webcam connection is lost
		bool blnFrameReadSuccessfully = capWebcam.read(imgOriginal);		// get next frame

		if (!blnFrameReadSuccessfully || imgOriginal.empty()) {		// if frame not read successfully
			std::cout << "error: frame not read from webcam\n";		// print error message to std out
			break;													// and jump out of while loop
		}

		cv::cvtColor(imgOriginal, imgHSV, cv::COLOR_BGR2HSV);

		// H: 0 - 179, S : 0 - 255, V : 0 - 255
		cv::inRange(imgHSV, cv::Scalar(0, 0, 0), cv::Scalar(179, 50, 255), imgThreshWhite); // low S, (high V)
		cv::inRange(imgHSV, cv::Scalar(0, 0, 0), cv::Scalar(179, 255, 60), imgThreshBlack);   // low V
		cv::inRange(imgHSV, cv::Scalar(36, 40, 20), cv::Scalar(85, 255, 255), imgThreshGreen);

		cv::add(imgThreshWhite, imgThreshBlack, imgSegmentation);
		cv::add(imgThreshGreen, imgSegmentation, imgSegmentation);

		cv::bitwise_not(imgSegmentation, imgSegmentation); // invert mask to select the object 

		cv::GaussianBlur(imgSegmentation, imgSegmentation, cv::Size(3, 3), 0);

		/// just to check how it looks like before applying morphology
		cv::imshow("imgSegmentation without Morphology", imgSegmentation);
		
		// Closing (Morphological Transformation)
		cv::Mat structuringElement = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
		cv::dilate(imgSegmentation, imgSegmentation, structuringElement);
		cv::erode(imgSegmentation, imgSegmentation, structuringElement);

		// Opening
		//cv::erode(imgSegmentation, imgSegmentation, structuringElement);
		//cv::dilate(imgSegmentation, imgSegmentation, structuringElement);

		cv::imshow("imgOriginal", imgOriginal);			// show windows
		cv::imshow("imgSegmentation", imgSegmentation);

		charCheckForEscKey = cv::waitKey(1);			// delay (in ms) and get key press, if any
	}	// end while

	return(0);
}

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/src/Edge.cc" "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/pod-build/CMakeFiles/apriltags.dir/src/Edge.cc.o"
  "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/src/FloatImage.cc" "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/pod-build/CMakeFiles/apriltags.dir/src/FloatImage.cc.o"
  "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/src/GLine2D.cc" "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/pod-build/CMakeFiles/apriltags.dir/src/GLine2D.cc.o"
  "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/src/GLineSegment2D.cc" "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/pod-build/CMakeFiles/apriltags.dir/src/GLineSegment2D.cc.o"
  "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/src/Gaussian.cc" "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/pod-build/CMakeFiles/apriltags.dir/src/Gaussian.cc.o"
  "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/src/GrayModel.cc" "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/pod-build/CMakeFiles/apriltags.dir/src/GrayModel.cc.o"
  "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/src/Homography33.cc" "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/pod-build/CMakeFiles/apriltags.dir/src/Homography33.cc.o"
  "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/src/MathUtil.cc" "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/pod-build/CMakeFiles/apriltags.dir/src/MathUtil.cc.o"
  "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/src/Quad.cc" "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/pod-build/CMakeFiles/apriltags.dir/src/Quad.cc.o"
  "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/src/Segment.cc" "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/pod-build/CMakeFiles/apriltags.dir/src/Segment.cc.o"
  "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/src/TagDetection.cc" "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/pod-build/CMakeFiles/apriltags.dir/src/TagDetection.cc.o"
  "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/src/TagDetector.cc" "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/pod-build/CMakeFiles/apriltags.dir/src/TagDetector.cc.o"
  "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/src/TagFamily.cc" "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/pod-build/CMakeFiles/apriltags.dir/src/TagFamily.cc.o"
  "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/src/UnionFindSimple.cc" "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/apriltags/pod-build/CMakeFiles/apriltags.dir/src/UnionFindSimple.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "include"
  "../build/include"
  "../AprilTags"
  "../."
  "/opt/local/include"
  "/usr/local/include/opencv4"
  "/usr/include/eigen3"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpherePointGenerator 
{
    public static LinkedList<Vector3> Generate(int numberOfPoints, float radius, bool hemisphere)
    {
        if (hemisphere)
            numberOfPoints *= 2;
        LinkedList<Vector3> positions = new LinkedList<Vector3>();
        float phi_k_last = 0;
        float tmpConst = 3.809f / Mathf.Sqrt(numberOfPoints);
        for (int k = 1; k <= numberOfPoints; ++k)
        {
            float h_k = -1.0f + (2.0f * (k - 1)) / (numberOfPoints - 1);
            float theta_k = Mathf.Acos(h_k);
            float phi_k = 0;
            if (k != 1 && k != numberOfPoints)
            {
                phi_k = (phi_k_last + tmpConst * (1.0f / Mathf.Sqrt(1.0f - h_k * h_k))) % (2 * Mathf.PI);
            }

            Vector3 point = new Vector3(
                Mathf.Sin(theta_k) * Mathf.Cos(phi_k),
                h_k,
                Mathf.Sin(theta_k) * Mathf.Sin(phi_k)
                ) * radius;

            if (!hemisphere || point.y > 0)
            {
                positions.AddLast(point);
            }

            phi_k_last = phi_k;
        }
        return positions;
    }

    public static LinkedList<Vector3> GenerateLayeredPoints(int numberOfLayers, int pointsPerLayer, float radius, int layerLimit)
    {
        LinkedList<Vector3> positions = new LinkedList<Vector3>();
        for (int i = 0; i < numberOfLayers && i < layerLimit; ++i)
        {
            float angleUp = (float)(i + 1) / numberOfLayers * (Mathf.PI / 2);
            for (int ii = 0; ii < pointsPerLayer; ++ii)
            {
                float angle = (float) ii * (2 * Mathf.PI / pointsPerLayer);
                Vector3 position = new Vector3(Mathf.Sin(angle) * Mathf.Cos(angleUp), Mathf.Sin(angleUp), Mathf.Cos(angle) * Mathf.Cos(angleUp));
                positions.AddLast(position);
                // Only take a single screenshot from the top
                if (angleUp == Mathf.PI / 2)
                    break;
            }
        }

        return positions;
    }
}

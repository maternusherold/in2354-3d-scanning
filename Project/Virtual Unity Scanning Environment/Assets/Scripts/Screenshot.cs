﻿using System;
using UnityEngine;
using System.Collections;
using System.IO;

public class Screenshot : MonoBehaviour {
    public int resWidth = 1920; 
    public int resHeight = 1080;
 
    private bool takeScreenshot = false;
    private string identifier;
 
    public static string ScreenShotName(int width, int height, string identifier) {
        //return $"{Application.persistentDataPath}/screenshots/screen_{width}x{height}_{identifier}.png";
        return $"{Application.persistentDataPath}/screenshots/{identifier.PadLeft(2, '0')}.png";
    }
 
    public void TakeScreenshot(string identifier) {
        takeScreenshot = true;
        this.identifier = identifier;
    }
 
    void LateUpdate()
    {
        Camera camera = GetComponent<Camera>();
        takeScreenshot |= Input.GetKeyDown("k");
        if (takeScreenshot) {
            RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
            camera.targetTexture = rt;
            Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
            camera.Render();
            RenderTexture.active = rt;
            screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
            camera.targetTexture = null;
            RenderTexture.active = null; // JC: added to avoid errors
            Destroy(rt);
            byte[] bytes = screenShot.EncodeToPNG();
            string filename = ScreenShotName(resWidth, resHeight, this.identifier);
            DirectoryInfo directoryInfo = new DirectoryInfo(filename);
            if(!directoryInfo.Parent.Exists)
                directoryInfo.Parent.Create();
            FileStream file = File.Open(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            file.Write(bytes, 0, bytes.Length);
            Debug.Log($"Took screenshot to: {filename}");
            takeScreenshot = false;
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    public SpherePointGeneratorVisualizer spherePointGenerator;
    public Transform lookAtObject;
    public float timeToWait;
    public Screenshot screenshot;
    void Start()
    {
        StartCoroutine(IterateThroughPoints(spherePointGenerator.positions, lookAtObject));
    }

    IEnumerator IterateThroughPoints(LinkedList<Vector3> points, Transform lookAtPosition)
    {
        int index = 1;
        LinkedListNode<Vector3> current = points.First;
        while(current != null)
        {
            yield return new WaitForSeconds(timeToWait);
            transform.position = current.Value;
            transform.LookAt(lookAtPosition);
            screenshot.TakeScreenshot($"{index++}");
            current = current.Next;
        }

    }
}

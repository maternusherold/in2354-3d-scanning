﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpherePointGeneratorVisualizer : MonoBehaviour
{
    public int numberOfPoints;
    public float radius;
    public GameObject pointPrefab;
    public LinkedList<Vector3> positions;
    public bool renderPoints;
    public int numberOfLayers;
    public int layerLimit = 3;

    // Start is called before the first frame update
    void Awake()
    {
        //positions = SpherePointGenerator.Generate(numberOfPoints, radius, true);
        positions = SpherePointGenerator.GenerateLayeredPoints(numberOfLayers, numberOfPoints, radius, layerLimit);
        if (renderPoints)
        {
            foreach(Vector3 point in positions)
            {
                Instantiate(pointPrefab, transform.position + point, Quaternion.identity, transform);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

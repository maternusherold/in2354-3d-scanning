# AR Marker & Voxel Carving

This Project was created during the 3D scanning and motion capture lecture.

## Background

This project uses AprilTags. The code used in order to find apriltags in a camera inpt can be find [here](https://people.csail.mit.edu/kaess/apriltags/). A comprehensive insctruction in how to install and use AprilTags on its own can be found [here](https://www.orangenarwhals.com/2018/04/getting-started-with-apriltags-on-ubuntu-16-04/).

Python [package](https://github.com/smidm/video2calibration) for camera calibration. The usage of this calibration method alongside apriltags is also explained [here](https://www.orangenarwhals.com/2018/04/getting-started-with-apriltags-on-ubuntu-16-04/).


## Installation

Libraries needed by Apriltags detection code:

```
sudo apt-get install subversion cmake libopencv-dev libeigen3-dev libv4l-dev
```
Libraries needed for the camera calibration: numpy, PyYAML

After installing the needed libraries, this project then can be simply pulled to an arbitrary folder.

Then you need to build the apriltag demo project by:
```
cd apriltags
make
./build/bin/apriltags_demo
```

## Usage

### Camera calibration

First print the checkerboard pattern from ```video2calibration/pattern.png```.

Go to the folder ```video2calibration```

Use the camera calibration (run script ./calibrate.py)
use ```$ ./calibrate.py --help``` to see usage.

Save your calibration video to ```video2calibration/input```.

Run the camera calibration for example by: 
```
./calibrate.py input/calibration_video_damian_logitech_webcam.avi output/calibration_damian_logitech_webcam.yaml --debug-dir out
```

Check if the camera calibration worked by running ```./undistorted``` for example by: 
```
./undistort.py output/calibration_damian_logitech_webcam.yaml  'input/distorted_damian.png' out/
```
Then edit ```/projects/apriltags/example$ vi apriltags_demo.cpp``` and add the calibration parameters.

### Apriltag detection

First print Apriltags from ```apriltags/tags/tag36h11.pdf```.

Run ```./build/bin/apriltags_demo```

Now a video frame of your webcam should open

## License
??? [MIT](https://choosealicense.com/licenses/mit/) ???

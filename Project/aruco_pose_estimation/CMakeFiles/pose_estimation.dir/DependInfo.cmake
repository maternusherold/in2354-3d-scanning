# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/pose_estimation/src/main.cpp" "/home/damian/Dokumente/WiSe2021/3DscanningMotionCapture/in2354-3d-scanning/Project/pose_estimation/CMakeFiles/pose_estimation.dir/src/main.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CERES_FOUND"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "include"
  "/usr/local/include/opencv4"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

/*
 * Copyright (c) 2019 Flight Dynamics and Control Lab
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include <opencv2/opencv.hpp>
#include <opencv2/aruco.hpp>
#include <iostream>
#include <cstdlib>
#include <eigen3/Eigen/Dense>
// #include <opencv2/core/eigen.hpp>


using namespace std;

// dirty workaround
#define CV_AA cv::LINE_AA

// tag positions relative to 0 tag (and world coord. frame)
Eigen::Vector3d tag1pos;
Eigen::Vector3d tag2pos;
Eigen::Vector3d tag3pos;
// Eigen::Vector3d tag1pos(1,0,0);
// Eigen::Vector3d tag2pos(1,1,0);
// Eigen::Vector3d tag3pos(1,0,0);

// camera calibration
cv::Mat camera_matrix;
cv::Mat dist_coeffs;

double marker_length_m;
int dictionaryId;

cv::String videoInput;
int wait_time;

void init()
{
    tag1pos << 0.15,0,0;
    tag2pos << 0.15,0.15,0;
    tag3pos << 0.15,0,0;
    camera_matrix = (cv::Mat_<double>(3,3) << 1, 0, 0, 0, 1, 0, 0, 0, 1);
    dist_coeffs = (cv::Mat_<double>(5,1) << 0, 0, 0,0,0);
    marker_length_m = 0.042;
    dictionaryId = 16;

    wait_time = 1000;
    videoInput = "2";
}


Eigen::Matrix3d createRotMatrix(Eigen::Vector3d x, Eigen::Vector3d y)  
{  
    Eigen::Matrix3d rot;
    rot.col(0) = x.normalized();  
    rot.col(2) = x.cross(y).normalized();

    // calculating y by cross product ensures perfect correspondence
    Eigen::Vector3d y_new = rot.col(2).cross(x);
    rot.col(1) = y_new.normalized();
    return rot;
}


// namespace {
// const char* about = "Pose estimation of ArUco marker images";
// const char* keys  =
//         "{d        |16    | dictionary: DICT_4X4_50=0, DICT_4X4_100=1, "
//         "DICT_4X4_250=2, DICT_4X4_1000=3, DICT_5X5_50=4, DICT_5X5_100=5, "
//         "DICT_5X5_250=6, DICT_5X5_1000=7, DICT_6X6_50=8, DICT_6X6_100=9, "
//         "DICT_6X6_250=10, DICT_6X6_1000=11, DICT_7X7_50=12, DICT_7X7_100=13, "
//         "DICT_7X7_250=14, DICT_7X7_1000=15, DICT_ARUCO_ORIGINAL = 16}"
//         "{h        |false | Print help }"
//         "{l        |      | Actual marker length in meter }"
//         "{v        |<none>| Custom video source, otherwise '0' }"
//         "{h        |false | Print help }"
//         "{l        |      | Actual marker length in meter }"
//         "{v        |<none>| Custom video source, otherwise '0' }"
//         ;
// }

int main(int argc, char **argv)
{
    
    init();
    // cv::CommandLineParser parser(argc, argv, keys);
    // parser.about(about);

    // if (argc < 2) {
    //     parser.printMessage();
    //     return 1;
    // }

    // if (parser.get<bool>("h")) {
    //     parser.printMessage();
    //     return 0;
    // }

    // int dictionaryId = parser.get<int>("d");
    // float marker_length_m = parser.get<float>("l");
    // int wait_time = 10;

    // if (marker_length_m <= 0) {
    //     std::cerr << "marker length must be a positive value in meter" 
    //               << std::endl;
    //     return 1;
    // }

    // cv::String videoInput = "0";
    cv::VideoCapture in_video;
    in_video.open(2);

    // if (parser.has("v")) {
    //     videoInput = parser.get<cv::String>("v");
    //     if (videoInput.empty()) {
    //         parser.printMessage();
    //         return 1;
    //     }
    //     char* end = nullptr;
    //     int source = static_cast<int>(std::strtol(videoInput.c_str(), &end, \
    //         10));
    //     if (!end || end == videoInput.c_str()) {
    //         in_video.open(videoInput); // url
    //     } else {
    //         in_video.open(source); // id
    //     }
    // } else {
    //     // use second camera:
    //     in_video.open(0);
    // }

    // if (!parser.check()) {
    //     parser.printErrors();
    //     return 1;
    // }

    if (!in_video.isOpened()) {
        std::cerr << "failed to open video input: " << videoInput << std::endl;
        return 1;
    }

    cv::Mat image, image_copy;
    // cv::Mat camera_matrix, dist_coeffs;
    std::ostringstream vector_to_marker;

    cv::Ptr<cv::aruco::Dictionary> dictionary =
        cv::aruco::getPredefinedDictionary( \
        cv::aruco::PREDEFINED_DICTIONARY_NAME(dictionaryId));

    // specify calibration location:
    // cv::FileStorage fs("./calibration_damian_logitech_webcam.yaml", cv::FileStorage::READ);

    // fs["camera_matrix"] >> camera_matrix;
    // fs["distortion_coefficients"] >> dist_coeffs;

    // std::cout << "camera_matrix\n" << camera_matrix << std::endl;
    // std::cout << "\ndist coeffs\n" << dist_coeffs << std::endl;

    while (in_video.grab())
    {
        in_video.retrieve(image);
        image.copyTo(image_copy);
        std::vector<int> ids;
        std::vector<std::vector<cv::Point2f> > corners;
        cv::aruco::detectMarkers(image, dictionary, corners, ids);

        // tag positions
        Eigen::Vector3d tag0;
        Eigen::Vector3d tag1;
        Eigen::Vector3d tag2;
        Eigen::Vector3d tag3;

        // trick: subtract 6 with index of tag in order to identify which of the four tags cannot be seen
        int unseentag = 6;

        if (ids.size() == 3)
        {
            std::vector<cv::Vec3d> rvecs, tvecs;
            cv::aruco::estimatePoseSingleMarkers(corners, marker_length_m,
                    camera_matrix, dist_coeffs, rvecs, tvecs);

            for(int i=0; i < ids.size(); i++)
            {
                // cout << ids[i] << " " << tvecs[i] << endl;
                switch (ids[i])
                {
                    case 0:
                        tag0(0)=tvecs[i][0];
                        tag0(1)=tvecs[i][1];
                        tag0(2)=tvecs[i][2];
                        cout << " tag0: " << tag0 << endl;
                        break;
                    case 1:
                        tag1(0)=tvecs[i][0];
                        tag1(1)=tvecs[i][1];
                        tag1(2)=tvecs[i][2];
                        // cout << " tag1: " << tag1 << endl;
                        unseentag -= 1;
                        break;
                    case 2:
                        tag2(0)=tvecs[i][0];
                        tag2(1)=tvecs[i][1];
                        tag2(2)=tvecs[i][2];
                        // cout << " tag2: " << tag2 << endl;
                        unseentag -= 2;
                        break;
                    case 3:
                        tag3(0)=tvecs[i][0];
                        tag3(1)=tvecs[i][1];
                        tag3(2)=tvecs[i][2];
                        // cout << " tag3: " << tag3 << endl;
                        unseentag -= 3;
                        break;
                    default:
                        cout << "tag with wrong id identified! (only use tags 0-3)" << endl;
                }        
            }
        } else {
            cout << "not detected exactly three markers!" << endl;
        }

        // calculate vector x and y denoting coord. system 
        // set transformation from current center tag to world tag 
        Eigen::Vector3d transformtoworld;
        Eigen::Vector3d x;
        Eigen::Vector3d y;

        Eigen::Vector3d worldposition;
        worldposition << 0,0,0;

        Eigen::Matrix4d worldrot;

        switch (unseentag)
                {
                    case 2:
                        // tag 0 center
                        x = tag1-tag0;
                        y = tag3-tag0;
                        worldrot.block<3,1>(0,3) = tag0;
                        break;
                    case 3:
                        worldposition += tag1pos;
                        x = -1*(tag1-tag0);
                        y = tag2-tag1;
                        worldrot.block<3,1>(0,3) = tag1;
                        break;
                    case 0:
                        worldposition += tag2pos;
                        x = -1*(tag3-tag2);
                        y = -1*(tag1-tag2);
                        worldrot.block<3,1>(0,3) = tag2;
                        break;
                    case 1:
                        worldposition += tag3pos;
                        x = (tag2-tag3);
                        y = -1*(tag1-tag3);
                        worldrot.block<3,1>(0,3) = tag3;
                        break;
                    default:
                        cout << "nononono you shall not pass!" << endl;
                }

        Eigen::Vector4d worldposition4;
        worldposition4 << worldposition[0],worldposition[1],worldposition[2],0;

        worldrot.block<3,3>(0,0) = createRotMatrix(x,y);

        // Eigen::Vector4d pose = worldrot;



        // cout << "x " << x << endl;
        // cout << "y " << y << endl;


        // cout << createRotMatrix(x,y).inverse()*tag0 << endl;


        // if at least one marker detected
        // if (ids.size() > 0)
        // {
        //     cv::aruco::drawDetectedMarkers(image_copy, corners, ids);
        //     std::vector<cv::Vec3d> rvecs, tvecs;
        //     cv::aruco::estimatePoseSingleMarkers(corners, marker_length_m,
        //             camera_matrix, dist_coeffs, rvecs, tvecs);

            

            
        //     // Draw axis for each marker
        //     for(int i=0; i < ids.size(); i++)
        //     {
        //         cv::aruco::drawAxis(image_copy, camera_matrix, dist_coeffs,
        //                 rvecs[i], tvecs[i], 0.1);


        //         cout << ids[i] << " " << tvecs[i] << endl;

        //         // This section is going to print the data for all the detected
        //         // markers. If you have more than a single marker, it is
        //         // recommended to change the below section so that either you
        //         // only print the data for a specific marker, or you print the
        //         // data for each marker separately.
        //         vector_to_marker.str(std::string());
        //         vector_to_marker << std::setprecision(4)
        //                          << "x: " << std::setw(8) << tvecs[0](0);
        //         cv::putText(image_copy, vector_to_marker.str(),
        //                     cv::Point(10, 30), cv::FONT_HERSHEY_SIMPLEX, 0.6,
        //                     cv::Scalar(0, 252, 124), 1, CV_AA);

        //         vector_to_marker.str(std::string());
        //         vector_to_marker << std::setprecision(4)
        //                          << "y: " << std::setw(8) << tvecs[0](1);
        //         cv::putText(image_copy, vector_to_marker.str(),
        //                     cv::Point(10, 50), cv::FONT_HERSHEY_SIMPLEX, 0.6,
        //                     cv::Scalar(0, 252, 124), 1, CV_AA);

        //         vector_to_marker.str(std::string());
        //         vector_to_marker << std::setprecision(4)
        //                          << "z: " << std::setw(8) << tvecs[0](2);
        //         cv::putText(image_copy, vector_to_marker.str(),
        //                     cv::Point(10, 70), cv::FONT_HERSHEY_SIMPLEX, 0.6,
        //                     cv::Scalar(0, 252, 124), 1, CV_AA);
        //     }
        // }

        imshow("Pose estimation", image_copy);
        char key = (char)cv::waitKey(10);
        if (key == 27)
            break;
    }

    in_video.release();

    return 0;
}

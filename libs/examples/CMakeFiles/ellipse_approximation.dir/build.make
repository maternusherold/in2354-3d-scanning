# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.19

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Disable VCS-based implicit rules.
% : %,v


# Disable VCS-based implicit rules.
% : RCS/%


# Disable VCS-based implicit rules.
% : RCS/%,v


# Disable VCS-based implicit rules.
% : SCCS/s.%


# Disable VCS-based implicit rules.
% : s.%


.SUFFIXES: .hpux_make_needs_suffix_list


# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs/ceres-solver

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs

# Include any dependencies generated for this target.
include examples/CMakeFiles/ellipse_approximation.dir/depend.make

# Include the progress variables for this target.
include examples/CMakeFiles/ellipse_approximation.dir/progress.make

# Include the compile flags for this target's objects.
include examples/CMakeFiles/ellipse_approximation.dir/flags.make

examples/CMakeFiles/ellipse_approximation.dir/ellipse_approximation.cc.o: examples/CMakeFiles/ellipse_approximation.dir/flags.make
examples/CMakeFiles/ellipse_approximation.dir/ellipse_approximation.cc.o: ceres-solver/examples/ellipse_approximation.cc
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object examples/CMakeFiles/ellipse_approximation.dir/ellipse_approximation.cc.o"
	cd /home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs/examples && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/ellipse_approximation.dir/ellipse_approximation.cc.o -c /home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs/ceres-solver/examples/ellipse_approximation.cc

examples/CMakeFiles/ellipse_approximation.dir/ellipse_approximation.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/ellipse_approximation.dir/ellipse_approximation.cc.i"
	cd /home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs/examples && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs/ceres-solver/examples/ellipse_approximation.cc > CMakeFiles/ellipse_approximation.dir/ellipse_approximation.cc.i

examples/CMakeFiles/ellipse_approximation.dir/ellipse_approximation.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/ellipse_approximation.dir/ellipse_approximation.cc.s"
	cd /home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs/examples && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs/ceres-solver/examples/ellipse_approximation.cc -o CMakeFiles/ellipse_approximation.dir/ellipse_approximation.cc.s

# Object files for target ellipse_approximation
ellipse_approximation_OBJECTS = \
"CMakeFiles/ellipse_approximation.dir/ellipse_approximation.cc.o"

# External object files for target ellipse_approximation
ellipse_approximation_EXTERNAL_OBJECTS =

bin/ellipse_approximation: examples/CMakeFiles/ellipse_approximation.dir/ellipse_approximation.cc.o
bin/ellipse_approximation: examples/CMakeFiles/ellipse_approximation.dir/build.make
bin/ellipse_approximation: lib/libceres.a
bin/ellipse_approximation: /usr/lib/libglog.so
bin/ellipse_approximation: /usr/lib/libspqr.so
bin/ellipse_approximation: /usr/lib/libtbb.so
bin/ellipse_approximation: /usr/lib/libcholmod.so
bin/ellipse_approximation: /usr/lib/libccolamd.so
bin/ellipse_approximation: /usr/lib/libcamd.so
bin/ellipse_approximation: /usr/lib/libcolamd.so
bin/ellipse_approximation: /usr/lib/libamd.so
bin/ellipse_approximation: /usr/lib/liblapack.so
bin/ellipse_approximation: /usr/lib/libblas.so
bin/ellipse_approximation: /usr/lib/libsuitesparseconfig.so
bin/ellipse_approximation: /usr/lib/librt.so
bin/ellipse_approximation: /usr/lib/libmetis.so
bin/ellipse_approximation: /usr/lib/libcxsparse.so
bin/ellipse_approximation: /usr/lib/liblapack.so
bin/ellipse_approximation: /usr/lib/libblas.so
bin/ellipse_approximation: /usr/lib/libsuitesparseconfig.so
bin/ellipse_approximation: /usr/lib/librt.so
bin/ellipse_approximation: /usr/lib/libmetis.so
bin/ellipse_approximation: /usr/lib/libcxsparse.so
bin/ellipse_approximation: examples/CMakeFiles/ellipse_approximation.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX executable ../bin/ellipse_approximation"
	cd /home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs/examples && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/ellipse_approximation.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
examples/CMakeFiles/ellipse_approximation.dir/build: bin/ellipse_approximation

.PHONY : examples/CMakeFiles/ellipse_approximation.dir/build

examples/CMakeFiles/ellipse_approximation.dir/clean:
	cd /home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs/examples && $(CMAKE_COMMAND) -P CMakeFiles/ellipse_approximation.dir/cmake_clean.cmake
.PHONY : examples/CMakeFiles/ellipse_approximation.dir/clean

examples/CMakeFiles/ellipse_approximation.dir/depend:
	cd /home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs/ceres-solver /home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs/ceres-solver/examples /home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs /home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs/examples /home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs/examples/CMakeFiles/ellipse_approximation.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : examples/CMakeFiles/ellipse_approximation.dir/depend


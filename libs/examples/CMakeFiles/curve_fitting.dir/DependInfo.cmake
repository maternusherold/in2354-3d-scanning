# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs/ceres-solver/examples/curve_fitting.cc" "/home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs/examples/CMakeFiles/curve_fitting.dir/curve_fitting.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CERES_EXPORT_INTERNAL_SYMBOLS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "config"
  "ceres-solver/include"
  "/usr/include/eigen3"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs/internal/ceres/CMakeFiles/ceres.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

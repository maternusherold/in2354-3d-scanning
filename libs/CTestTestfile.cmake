# CMake generated Testfile for 
# Source directory: /home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs/ceres-solver
# Build directory: /home/maternus/GDrive/01_Documents/01_Education/04_Masters/IN2354_3DScanning/exercises/exercise-projects/libs
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("internal/ceres")
subdirs("examples")
